import org.knowm.xchart.Chart_XY;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.internal.chartpart.Chart;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ghost on 02.03.16.
 */
public class ExampleVlad0 {
    private Chart chart;
    public static final String SERIES_NAME = "series1";
    private List<Integer> xData;
    private List<Double> yData;

    public static void main(String[] args) {
        final ExampleVlad0 realtimeChart02 = new ExampleVlad0();
        final XChartPanel chartPanel = realtimeChart02.buildPanel();
        SwingUtilities.invokeLater(new Runnable(){

            @Override
            public void run() {
                JFrame frame = new JFrame("XChart");
                frame.setDefaultCloseOperation(3);
                frame.add((Component)chartPanel);
                frame.pack();
                frame.setVisible(true);
            }
        });
        TimerTask chartUpdaterTask = new TimerTask(){

            @Override
            public void run() {
                realtimeChart02.updateData();
                chartPanel.updateSeries("series1", realtimeChart02.getxData(), realtimeChart02.getyData(), null);
            }
        };
        java.util.Timer timer = new java.util.Timer();
        timer.scheduleAtFixedRate(chartUpdaterTask, 0, 500);
    }

    public XChartPanel buildPanel() {
        return new XChartPanel(this.getChart());
    }
    public Chart getChart() {
        this.xData = this.getMonotonicallyIncreasingData(5);
        this.yData = this.getRandomData(5);
        Chart_XY chart = new Chart_XY(500, 400);
        chart.setTitle("Sample Real-time Chart");
        chart.setXAxisTitle("X");
        chart.setYAxisTitle("Y");
        chart.addSeries("series1", this.xData, this.yData);
        return chart;
    }
    private List<Integer> getMonotonicallyIncreasingData(int numPoints) {
        CopyOnWriteArrayList<Integer> data = new CopyOnWriteArrayList<Integer>();
        for (int i = 0; i < numPoints; ++i) {
            data.add(i);
        }
        return data;
    }
    private List<Double> getRandomData(int numPoints) {
        CopyOnWriteArrayList<Double> data = new CopyOnWriteArrayList<Double>();
        for (int i = 0; i < numPoints; ++i) {
            data.add(Math.random() * 100.0);
        }
        return data;
    }
    public void updateData() {
        List<Double> newData = this.getRandomData(1);
        this.yData.addAll(newData);
        while (this.yData.size() > 20) {
            this.yData.remove(0);
        }
        this.xData.add(this.xData.get(this.xData.size() - 1) + 1);
        while (this.xData.size() > 20) {
            this.xData.remove(0);
        }
    }

    public List<Double> getyData() {
        return this.yData;
    }

    public List<Integer> getxData() {
        return this.xData;
    }
}
