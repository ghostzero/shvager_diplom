import org.knowm.xchart.QuickChart;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.internal.chartpart.Chart;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ghost on 01.03.16.
 */
public class StartFile {

    public static View Grafik_form;
    public static JTextField newtextfield = new JTextField();
        public static Chart chart;

    public static void main(String[] args) {

        // Create Chart
        double[] xData = new double[]{0.0, 1.0, 2.0};
        double[] yData = new double[]{2.0, 1.0, 0.0};

        // Create Chart
      chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);

        // Create and set up the window.
        JFrame frame = new JFrame("Advanced Example");
        JLabel label = new JLabel("Blah blah blah.", SwingConstants.CENTER);
        JTextField nextnewtextfield = newtextfield;
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS)); // <-- you need this for now

                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                JPanel chartPanel = new XChartPanel(chart);
                frame.add(chartPanel);
                JButton button1 = new JButton("ok");
                button1.setText("text");
                button1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        button1ActionPerformed(e);
                    }
                });
                frame.add(button1);
                frame.add(nextnewtextfield);
                frame.add(label);


                // Display the window.
                frame.pack();
                frame.setVisible(true);

            }
        });
    }




    public void actionPerformed(ActionEvent ae) {

    }


    private static void button1ActionPerformed(ActionEvent e) {
        // TODO add your code here
//        System.out.println(newtextfield.getText());
        String str = newtextfield.getText();
        Integer dimX = 10;   //interval for function
        Integer emptyX = 2;  //empty interval
        Integer step = 0;
//        ArrayList UserArray=new ArrayList();
        float userarray[][] = new float[2][dimX];
        for (int x = 0; x < dimX; x++) {
            userarray[0][x] = x;
        }
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            switch (c) {
                case 'A': {
                    for (int x = 0; x < dimX; x++) {
                        userarray[1][x] = x*x;
                    }
                    break;
                }
                case 'B': {
for (int x = 0; x < dimX; x++) {
                        userarray[1][x] = (float) (50*Math.exp(-(x*x)/25));
                    }
                    break;
                }
                case 'C': {
for (int x = 0; x < dimX; x++) {
                        userarray[1][x] = -x*x*x/10;
                    }
                    break;
                }
                case 'D': {
for (int x = 0; x < dimX; x++) {
                        userarray[1][x] = 100/(x+1);
                    }
                    break;
                }
            }
        }
        double[] xData = new double[]{0.0, 3.0, -2.0};
        double[] yData = new double[]{5.0, 1.0, 8.0};
        chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);
    }


}
