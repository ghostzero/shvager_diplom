import org.knowm.xchart.ChartBuilder_XY;
import org.knowm.xchart.Chart_XY;
import org.knowm.xchart.Series_XY;
import org.knowm.xchart.SwingWrapper;
import org.knowm.xchart.internal.chartpart.Chart;
import org.knowm.xchart.internal.style.markers.SeriesMarkers;

import java.util.ArrayList;
import java.util.List;

/**
 * Create a Chart matrix
 *
 * @author timmolter
 */
public class Example2 {

    public static void main(String[] args) {

        int numCharts = 4;

        List<Chart> charts = new ArrayList<Chart>();

        for (int i = 0; i < numCharts; i++) {
            Chart_XY chart = new ChartBuilder_XY().xAxisTitle("X").yAxisTitle("Y").width(600).height(400).build();
            chart.getStyler().setYAxisMin(-10);
            chart.getStyler().setYAxisMax(10);
            Series_XY series = chart.addSeries("" + i, null, getRandomWalk(200));
            series.setMarker(SeriesMarkers.NONE);
            charts.add(chart);
        }
        new SwingWrapper(charts).displayChartMatrix();
    }

    /**
     * Generates a set of random walk data
     *
     * @param numPoints
     * @return
     */
    private static double[] getRandomWalk(int numPoints) {

        double[] y = new double[numPoints];
        y[0] = 0;
        for (int i = 1; i < y.length; i++) {
            y[i] = y[i - 1] + Math.random() - .5;
        }
        return y;
    }

}