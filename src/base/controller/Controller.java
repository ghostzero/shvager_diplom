package base.controller;

import base.userclasses.ArrayAlphabet;
import org.knowm.xchart.Chart_XY;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.internal.chartpart.Chart;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by ghost on 02.03.16.
 */
public class Controller implements Runnable {
    JFrame frame = new JFrame("Advanced Example");
    final XChartPanel chartPanel = this.buildPanel();
    public JTextField newtextfield = new JTextField();
    JButton button1 = new JButton("ok");
    private List<Integer> xData;
    private List<Double> yData;
   /* String str = newtextfield.getText();
    Integer dimX = 10;   //interval for function
    Integer emptyX = 2;  //empty interval
    Integer step = 0;*/

    public Controller() {
        this.run();
    }

    @Override
    public void run() {
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS)); // <-- you need this for now
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(chartPanel);

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button1ActionPerformed(e);
            }
        });
        frame.add(button1);
        frame.add(newtextfield);
        frame.pack();
        frame.setVisible(true);

    }

    private void button1ActionPerformed(ActionEvent e) {
//        System.out.println(newtextfield.getText());
        this.updateData3();
        chartPanel.updateSeries("series1", this.getxData(), this.getyData(), null);
    }

    public XChartPanel buildPanel() {
        return new XChartPanel(this.getChart());
    }

    public Chart getChart() {
        this.xData = this.getMonotonicallyIncreasingData(10);
        this.yData = this.getRandomData(10);
        Chart_XY chart = new Chart_XY(800, 400);
        chart.setTitle("Sample Real-time Chart");
        chart.setXAxisTitle("X");
        chart.setYAxisTitle("Y");
        chart.addSeries("series1", this.xData, this.yData);
        return chart;
    }

    private List<Integer> getMonotonicallyIncreasingData(int numPoints) {
        CopyOnWriteArrayList<Integer> data = new CopyOnWriteArrayList<Integer>();
        for (int i = 0; i < numPoints; ++i) {
            data.add(i);
        }
        return data;
    }

    private List<Double> getRandomData(int numPoints) {
        CopyOnWriteArrayList<Double> data = new CopyOnWriteArrayList<Double>();
        for (int i = 0; i < numPoints; ++i) {
//            data.add(Math.random() * 100.0);
            data.add(0.0);
        }
        return data;
    }

    public List<Double> getyData() {
        return this.yData;
    }

    public List<Integer> getxData() {
        return this.xData;
    }

    public void updateData() {
        List<Double> newData = this.getRandomData(1);
        this.yData.addAll(newData);
        while (this.yData.size() > 20) {
            this.yData.remove(0);
        }
        this.xData.add(this.xData.get(this.xData.size() - 1) + 1);
        while (this.xData.size() > 20) {
            this.xData.remove(0);
        }
    }

    public void updateData2() {
        this.xData.clear();
        this.yData.clear();
        String str = newtextfield.getText();
        Integer dimX = 10;   //interval for function
        Integer emptyX = 2;  //empty interval
        Integer step = 0;
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            for (int x = 0; x < dimX; x++) {
                this.xData.add(x + 10 * (i + 1));
            }
            switch (c) {
                case 'A': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (x * x));
                    }
                    break;
                }
                case 'B': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((50 * Math.exp(-(x * x) / 25)));
                    }
                    break;
                }
                case 'C': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (-x * x * x / 10));
                    }
                    break;
                }
                case 'D': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (100 / (x + 1)));
                    }
                    break;
                }
                case 'E': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (Math.sin(x)));
                    }
                    break;
                }
                case 'F': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (Math.cos(x)));
                    }
                    break;
                }
                case 'G': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (Math.tan(x)));
                    }
                    break;
                }
                case 'H': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (1.0 / Math.tan(x)));
                    }
                    break;
                }
                case 'I': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (Math.exp(x)));
                    }
                    break;
                }
                case 'L': {
                    for (int x = 0; x < dimX; x++) {
                        this.yData.add((double) (Math.exp(x)));
                    }
                    break;
                }
            }
        }

    }

    public void updateData3() {
        this.xData.clear();
        this.yData.clear();
        String str = newtextfield.getText();
        Integer dimX = 20;   //interval for function
        Integer emptyX = 2;  //empty interval
        Integer step = 0;
        ArrayAlphabet objectArrayAlphabet = new ArrayAlphabet();
        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);
            if (c != ' ') {
                for (int x = 0; x < dimX; x++) {
                    this.xData.add(x + dimX * (i + 1));
                }
                try {
                    for (int x = 0; x < dimX; x++) {

                        if (Character.UnicodeBlock.of(c).equals(Character.UnicodeBlock.CYRILLIC)) {
                            // contains Cyrillic
                            objectArrayAlphabet.callByName("function" + this.transliterateChar(Character.toUpperCase(c)), x);

                        } else {
                            objectArrayAlphabet.callByName("function" + Character.toUpperCase(c), x);
                        }


                        Double temp_variable = objectArrayAlphabet.getfunctionReturn();
                        this.yData.add(temp_variable);
                    }
                    objectArrayAlphabet.getfunctionReturn();
                } catch (Exception e) {
                    System.out.println(e);
                }
            } else continue;
        }
    }

    public static String transliterate(String message) {

        char[] abcCyr = {' ', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
                , 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Б', 'Э', 'Ю', 'Я'
                , 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
                , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        String[] abcLat = {" ", "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sch", "", "i", "", "e", "ju", "ja"
                , "A", "B", "V", "G", "D", "E", "E", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "Ts", "Ch", "Sh", "Sch", "", "I", "", "E", "Ju", "Ja"
                , "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
                , "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < message.length(); i++) {
            for (int x = 0; x < abcCyr.length; x++)
                if (message.charAt(i) == abcCyr[x]) {
                    builder.append(abcLat[x]);
                }
        }
        return builder.toString();

    }

    public static char transliterateChar(char c) {

        char[] abcCyr = {' ', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'
                , 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Б', 'Э', 'Ю', 'Я'
                , 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'
                , 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

        String[] abcLat = {" ", "a", "b", "v", "g", "d", "e", "e", "zh", "z", "i", "y", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u", "f", "h", "ts", "ch", "sh", "sch", "", "i", "", "e", "ju", "ja"
                , "A", "B", "V", "G", "D", "E", "E", "Zh", "Z", "I", "Y", "K", "L", "M", "N", "O", "P", "R", "S", "T", "U", "F", "H", "Ts", "Ch", "Sh", "Sch", "", "I", "", "E", "Ju", "Ja"
                , "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
                , "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        StringBuilder builder = new StringBuilder();
        char resultChar = 0;


        for (int x = 0; x < abcCyr.length; x++)
            if (c == abcCyr[x]) {
                resultChar = abcLat[x].charAt(0);
            }

        return resultChar;

    }

}
