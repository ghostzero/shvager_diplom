package base.userclasses;

import java.util.HashMap;

/**
 * Created by ghost on 19.04.16.
 */
public class ArrayAlphabet {
    private HashMap<String, String> arrayAlphabetEnglish = new HashMap<String, String>();
    private Double functionReturn;


    public ArrayAlphabet() {
        arrayAlphabetEnglish.put("A", "");
    }

    public ArrayAlphabet callByName(String funcName, int currentvalue) throws Exception {
        this.getClass().getDeclaredMethod(funcName, int.class).invoke(this, currentvalue);
        return this;
    }

    public Double getfunctionReturn() {
        return this.functionReturn;
    }

    public void functionA(int x) {
        this.functionReturn = (double) (x * x);
    }

    public void functionB(int x) {
        this.functionReturn = (50 * Math.exp(-(x * x) / 25));
    }

    public void functionC(int x) {
        this.functionReturn = (double) (-x * x * x / 10);
    }

    public void functionD(int x) {
        this.functionReturn = Math.sin(x);
    }

    public void functionE(int x) {
        this.functionReturn = Math.cos(x);
    }

    public void functionF(int x) {
        this.functionReturn = Math.tan(x);
    }

    public void functionG(int x) {
        this.functionReturn = 1.0 / Math.tan(x);
    }

    public void functionH(int x) {
        this.functionReturn = Math.sin(x) * Math.sin(x);
    }

    public void functionI(int x) {
        this.functionReturn = (double) (100 / (x + 1));
    }

    public void functionL(int x) {
        this.functionReturn = Math.sin(x) * Math.cos(x);
    }

    public void functionM(int x) {
        this.functionReturn = Math.tan(x) * Math.sin(x);
    }

    public void functionN(int x) {
        this.functionReturn = Math.acos(x);
    }

    public void functionO(int x) {
        this.functionReturn = Math.acos(x * x);
    }

    public void functionP(int x) {
        this.functionReturn = Math.acos(x * x * x);
    }

    public void functionQ(int x) {
        this.functionReturn = Math.asin(x);
    }

    public void functionR(int x) {
        this.functionReturn = Math.asin(x * x);
    }

    public void functionS(int x) {
        this.functionReturn = Math.asin(x * x * x);
    }

    public void functionT(int x) {
        this.functionReturn = Math.atan(x);
    }

    public void functionU(int x) {
        this.functionReturn = Math.atan(x * x);
    }

    public void functionV(int x) {
        this.functionReturn = Math.atan(x * x * x);
    }

    public void functionW(int x) {
        this.functionReturn = Math.tanh(x);
    }

    public void functionX(int x) {
        this.functionReturn = Math.cbrt(x);
    }

    public void functionY(int x) {
        this.functionReturn = Math.cosh(x);
    }

    public void functionZ(int x) {
        this.functionReturn = Math.sinh(x);
    }

    public void functionZh(int x) {
        this.functionReturn = Math.sinh(x);
    }

    public void functionTs(int x) {
        this.functionReturn = (double) (x * x);
    }

    public void functionCh(int x) {
        this.functionReturn = (50 * Math.exp(-(x * x) / 25));
    }

    public void functionSh(int x) {
        this.functionReturn = (double) (-x * x * x / 10);
    }

    public void functionSch(int x) {
        this.functionReturn = Math.sin(x);
    }

    public void functionJu(int x) {
        this.functionReturn = Math.cos(x);
    }

    public void functionJa(int x) {
        this.functionReturn = Math.tan(x);
    }

    public void functionrusG(int x) {
        this.functionReturn = 1.0 / Math.tan(x);
    }

    public void functionrusH(int x) {
        this.functionReturn = Math.sin(x) * Math.sin(x);
    }

    public void functionrusI(int x) {
        this.functionReturn = (double) (100 / (x + 1));
    }

    public void functionrusL(int x) {
        this.functionReturn = Math.sin(x) * Math.cos(x);
    }

    public void functionrusM(int x) {
        this.functionReturn = Math.tan(x) * Math.sin(x);
    }

    public void functionrusN(int x) {
        this.functionReturn = Math.acos(x);
    }

    public void functionrusO(int x) {
        this.functionReturn = Math.acos(x * x);
    }

    public void functionrusP(int x) {
        this.functionReturn = Math.acos(x * x * x);
    }

    public void functionrusQ(int x) {
        this.functionReturn = Math.asin(x);
    }

    public void functionrusR(int x) {
        this.functionReturn = Math.asin(x * x);
    }

    public void functionrusS(int x) {
        this.functionReturn = Math.asin(x * x * x);
    }

    public void functionrusT(int x) {
        this.functionReturn = Math.atan(x);
    }

    public void functionrusU(int x) {
        this.functionReturn = Math.atan(x * x);
    }

    public void functionrusV(int x) {
        this.functionReturn = Math.atan(x * x * x);
    }

    public void functionrusW(int x) {
        this.functionReturn = Math.tanh(x);
    }

    public void functionrusX(int x) {
        this.functionReturn = Math.cbrt(x);
    }

    public void functionrusY(int x) {
        this.functionReturn = Math.cosh(x);
    }

    public void functionrusZ(int x) {
        this.functionReturn = Math.sinh(x);
    }

}
