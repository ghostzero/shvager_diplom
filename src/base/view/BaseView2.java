package base.view;

import org.knowm.xchart.QuickChart;
import org.knowm.xchart.XChartPanel;
import org.knowm.xchart.internal.chartpart.Chart;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ghost on 02.03.16.
 */
public class BaseView2 {

    public BaseView2() {
        double[] xData = new double[]{0.0, 1.0, 2.0};
        double[] yData = new double[]{2.0, 1.0, 0.0};
        // Create Chart
        Chart chart = QuickChart.getChart("Sample Chart", "X", "Y", "y(x)", xData, yData);
        final JFrame frame = new JFrame("Advanced Example");
        JLabel label = new JLabel("Blah blah blah.", SwingConstants.CENTER);
        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS)); // <-- you need this for now

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel chartPanel = new XChartPanel(chart);
        JTextField newtextfield = new JTextField();
        frame.add(chartPanel);
        JButton button1 = new JButton("ok");
        button1.setText("text");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button1ActionPerformed(e);
            }
        });
        frame.add(button1);
        frame.add(newtextfield);
        frame.add(label);


        // Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    private void button1ActionPerformed(ActionEvent e) {

    }

}
